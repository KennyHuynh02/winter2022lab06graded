import java.util.Scanner;
public class TicTacToeGame {
	public static void main(String[]args){
		Scanner keyboard = new Scanner(System.in);

		System.out.println("welcome to the Tic Tac Toe game!");
		System.out.println("Player 1's token: x");
		System.out.println("Player 2's token: O");
		Board board = new Board();
		
		boolean gameOver = false;
		int player = 1;
		Square playerToken = Square.X;
		
		while(!gameOver){
			System.out.println("");
			System.out.println(board);
			
			if (player == 1){
				playerToken = Square.X;
			}
			else{
				playerToken = Square.O;
			}
			
			System.out.println("Player " + player + " it's your turn, where do you wan to place your token?");
			System.out.println("");
			
			int userInputRow;
			int userInputCol;
			
			do {
				System.out.println("input a valid row that is from a Blank space");
				userInputRow = keyboard.nextInt();
				System.out.println("input a valid column that is from a Blank space");
				userInputCol = keyboard.nextInt();	
				System.out.println("");
			}	while (!board.placeToken(userInputRow, userInputCol, playerToken));
			
			if (board.checkIfWinning(playerToken)){
				System.out.println(board);
				System.out.println("player " + player + " is the winner");
				gameOver = true;
			}
			
			else if (board.checkIfFull()){
				System.out.println(board);
				System.out.println("It's a tie!");
				gameOver =true;
			}
			
			else {
				player ++;
				if(player > 2){
					player =1;
				}
			}
		}
	}
}