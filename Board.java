public class Board {
	private Square[][] tictactoeBoard;
	
	public Board(){
		this.tictactoeBoard = new Square[3][3];
		for (int i =0; i<tictactoeBoard.length; i++){
			for (int j = 0; j <tictactoeBoard.length; j++){
				tictactoeBoard[i][j] = Square.BLANK;
			}
		}
	}
	
	public String toString(){
		String board = "";
		for (int i =0; i<tictactoeBoard.length; i++){
			for (int j = 0; j <tictactoeBoard.length; j++){
				board += tictactoeBoard[i][j];
				board += " ";
			}
			board += "\n";
		}
		return board;
	}
	
	public boolean placeToken(int row, int col, Square playerToken){
		if (row <= 0 || row > 3){
			return false;
		}
		if (col <= 0 || col > 3){
			return false;
		}
		
		if(tictactoeBoard[row-1][col-1] ==Square.BLANK){
			tictactoeBoard[row-1][col-1] = playerToken;
			return true;
		}
		return false;
	}
	
	public boolean checkIfFull (){
		for (int i =0; i<tictactoeBoard.length; i++){
			for (int j = 0; j <tictactoeBoard.length; j++){
				if(tictactoeBoard[i][j] == Square.BLANK){
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken){
		for (int i =0; i<tictactoeBoard.length; i++){
			int sameTokenRow = 0;
			for (int j = 0; j <tictactoeBoard.length; j++){
				if(tictactoeBoard[i][j] == playerToken){
					sameTokenRow++;
				}
			}
			if(sameTokenRow == tictactoeBoard.length){
				return true;
			}
		}
		return false;
	}
	
	private boolean checkIfWinningVertical(Square playerToken){
		for (int i =0; i<tictactoeBoard.length; i++){
			int sameTokenRow = 0;
			for (int j = 0; j <tictactoeBoard.length; j++){
				if(tictactoeBoard[j][i] == playerToken){
					sameTokenRow++;
				}
			}
			if(sameTokenRow == tictactoeBoard.length){
				return true;
			}
		}
		return false;
	}
	
	public boolean checkIfWinning(Square playerToken){
		if (checkIfWinningHorizontal(playerToken)){
			return true;
		}
		if (checkIfWinningVertical(playerToken)){
			return true;
		}
		return false;
	}
}

